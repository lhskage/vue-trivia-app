import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import StartScreen from './components/StartScreen'
import Questions from './components/Questions'
import ResultScreen from './components/ResultScreen'

Vue.use(VueRouter);

const routes = [
  {
    path: '/', component: StartScreen,
    name: 'start'
  },
  {
    path: '/questions', component: Questions,
    name: 'questions'
  },
  {
    path: '/result', component: ResultScreen,
    name: 'result'
  }
];

const router = new VueRouter({ routes })

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
